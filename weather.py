import socket
import datetime

LETTERS_START = ord("a")
IP = "34.218.16.79"
PORT = 77
MSG = '''
Welcome to the weather service, please choose option
1 - get the weather today
2 - get the weather for three days
'''

def get_checksum(city, date):
    """
    The function get the city and the date, and calculate the checksum

    :param city: the city (str)
    :param date: the date (str)
    :return: the checksum
    """
    right = 0
    left = 0

    city = city.replace(" ", "")
    for letter in city.lower():
        right += ord(letter) - LETTERS_START + 1

    date = date.replace("/", "")
    date = date.replace("0", "")
    left = 0
    for i in date:
        left += int(i)

    return str(right) + "." + str(left)


def get_weather(city, date, checksum):
    """
    The function get all the details and get the answer from the server

    :param city: the current city
    :param date: the current date
    :return: the answer from the server
    """
    pattern = f"100:REQUEST:city={city}&date={date}&checksum={checksum}"

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.connect((IP, PORT))

        '''ignore from the first msg'''
        msg = sock.recv(1024).decode()
        sock.send(pattern.encode())
        msg = sock.recv(1024).decode()

    return msg


def analyze_answer(ans):
    """
    The function get the answer from the server, and analyze it

    :param ans: the answer (str)
    :return: (temperature, description)
    """
    if "ERROR" in ans:
        return 999, ans[ans.find("ERROR") + 6:]

    dex = ans.find("temp=") + 5
    temp = ''
    while ans[dex] != "&":
        temp += ans[dex]
        dex += 1
    return float(temp), ans[ans.find("text=") + 5:]


def main(city, date=datetime.datetime.today().strftime('%d/%m/%Y')):
    checksum = get_checksum(city, date)
    ans = get_weather(city, date, checksum)
    return analyze_answer(ans)


if __name__ == "__main__":
    option = int(input(MSG))
    run = []
    if option == 1:
        run.append(main(input("Enter city: ")))

    if option == 2:
        today = datetime.datetime.now()
        tomorrow = today + datetime.timedelta(days=1)
        tomorrow_of_tomorrow = today + datetime.timedelta(days=2)
        city = input('Enter city: ')
        run.append(main(city, today.strftime('%d/%m/%Y')))
        run.append(main(city, tomorrow.strftime('%d/%m/%Y')))
        run.append(main(city, tomorrow_of_tomorrow.strftime('%d/%m/%Y')))

    for temp, text in run:
        print(f'The current weather is:\n{temp} - {text}')
