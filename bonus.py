import weather


def get_city():
    """
    The function return list of citys
    :return: list of citys
    """
    data = open('capitals20.txt', 'r').readlines()

    for index, i in enumerate(data):
        data[index] = i.split(',')[2]

    return data


def get_temperature(citys):
    """
    The function map the citys with their temps
    :param citys: the list of citys
    :return: list of temps
    """
    temp = []
    for city in citys:
        temp.append(weather.main(city)[0])

    return temp


def sort_by_temp(citys, temp):
    """
    The function sorted the citys by their temps
    :param citys: list of citys
    :param temp: list of temp
    :return: None
    """

    i = 0

    while temp.count(999) > 0:
        dex = temp.index(999)
        citys.pop(dex)
        temp.pop(dex)

    while len(temp) > 0:
        i += 1
        dex = temp.index(max(temp))
        print(f'{i}. {citys[dex]} {temp[dex]} degrees\n')
        temp.pop(dex)
        citys.pop(dex)


def main():
    print('Please wait\n')
    capitals = get_city()
    temp = get_temperature(capitals)
    sort_by_temp(capitals, temp)


if __name__ == "__main__":
    main()
